package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 07.12.2021.
 */
public interface ILoggerService {

    void info(@Nullable final String message);

    void debug(@Nullable final String message);

    void command(@Nullable final String message);

    void error(@Nullable final Exception e);

}
