package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 20.12.2021.
 */
public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password);

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    );

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User findByEmail(@Nullable final String email);

    @Nullable
    User removeByLogin(@Nullable final String login);

    @Nullable
    User removeByEmail(@Nullable final String email);

    @Nullable
    User setPassword(@Nullable final String id,
                     @Nullable final String password);

    @Nullable
    User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    );

    @Nullable
    Boolean isLoginExist(@Nullable final String login);

    @Nullable
    Boolean isEmailExist(@Nullable final String email);

    void lockUserByLogin(@Nullable final String login);

    void unlockUserByLogin(@Nullable final String login);

}
