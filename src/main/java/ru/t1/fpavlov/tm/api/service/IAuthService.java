package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public interface IAuthService {

    @NotNull
    User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    void login(@Nullable final String login, @Nullable final String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@NotNull final Role[] roles);

}
