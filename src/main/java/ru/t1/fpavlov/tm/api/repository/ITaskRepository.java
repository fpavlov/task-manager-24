package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    Task create(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId);

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final Comparator comparator
    );

}
