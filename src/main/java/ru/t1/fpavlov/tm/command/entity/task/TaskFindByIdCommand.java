package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskFindByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Find Task by id and then display it";

    @NotNull
    public static final String NAME = "task-find-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Task entity = this.findById();
        System.out.println(entity);
    }

}
