package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 24.01.2022.
 */
public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove user";

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIB};
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String login = this.input("Login:");
        this.getUserService().removeByLogin(login);
    }

}
