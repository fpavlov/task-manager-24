package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IUserOwnedRepository;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fpavlov on 13.01.2022.
 */
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull List<M> items = this.findAll(userId);
        for (final M item : items) {
            this.remove(item);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        return this.findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        final List<M> items = this.findAll(userId);
        if (comparator == null) return items;
        items.sort(comparator);
        return items;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        final List<M> items = this.findAll(userId);
        if (sort == null) return items;
        items.sort(sort.getComparator());
        return items;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return this.findById(userId, id) != null;
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return this.findAll(userId)
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null || index < 0 || index > this.getSize(userId)) return null;
        return this.findAll(userId).get(index);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return this.findAll(userId).size();
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M item = this.findById(userId, id);
        return this.remove(item);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null || index < 0 || index > this.getSize(userId)) return null;
        @Nullable final M item = this.findByIndex(userId, index);
        return this.remove(item);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M item) {
        if (userId == null || item == null) return null;
        item.setUserId(userId);
        return this.add(item);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M item) {
        if (userId == null || item == null) return null;
        return this.removeById(userId, item.getId());
    }

}
