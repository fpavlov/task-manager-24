package ru.t1.fpavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * Created by fpavlov on 21.12.2021.
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractModel {

    @NotNull
    public static final String ID_FIELD_NAME = "id";

    @NotNull
    private String id = UUID.randomUUID().toString();

}
