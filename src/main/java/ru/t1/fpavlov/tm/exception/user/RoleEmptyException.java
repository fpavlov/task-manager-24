package ru.t1.fpavlov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class RoleEmptyException extends AbstractUserException {

    @NotNull
    public RoleEmptyException() {
        super("Error! Role is empty");
    }

}